package breakout;

import breakout.controllers.GameController;
import breakout.controllers.SoundController;
import breakout.models.BallModel;
import breakout.models.MapModel;
import breakout.models.PaddleModel;
import breakout.panels.GameView;
import breakout.panels.WelcomePanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class Main extends JFrame {
    private static JPanel cardPanel;
    private static CardLayout layout;
    private static GameController controller;
    private static long count = 0;
    private static int div;
    private final MapModel map;
    private final GameView view;
    private final BallModel ball;
    private final PaddleModel paddle;

    public Main() {
        layout = new CardLayout();
        cardPanel = new JPanel();
        cardPanel.setLayout(layout);
        add(cardPanel);

        div = 10;
        ball = new BallModel();
        map = new MapModel();
        paddle = new PaddleModel();
        controller = new GameController(map, ball, paddle);
        view = new GameView(map, ball, paddle, controller);
        controller.setView(view);

        Timer timer = new javax.swing.Timer(1, actionEvent -> {
            if (count % div == 0) {
                controller.run();
            }
            count += 1;

            if (count > 1000000) {
                count = 0;
            }

            if (controller.getScore() == 50) {
                div -= 1;
                controller.setScore(55);
            }

            if (controller.getScore() == 100) {
                div -= 1;
                controller.setScore(105);
            }
        });

        timer.start();

        WelcomePanel welcomePanel = new WelcomePanel();

        welcomePanel.getEasyButton().addActionListener(actionEvent -> playEasy());
        welcomePanel.getMediumButton().addActionListener(actionEvent -> playMedium());
        welcomePanel.getHardButton().addActionListener(actionEvent -> playHard());
        addCardPanel(welcomePanel, "Welcome");
        showPanel("Welcome");

        cardPanel.getInputMap().put(KeyStroke.getKeyStroke("LEFT"), "left");
        cardPanel.getInputMap().put(KeyStroke.getKeyStroke("RIGHT"), "right");
        cardPanel.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "repeat");
        cardPanel.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "repeat");
        cardPanel.getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "back");
        cardPanel.getActionMap().put("left", new GoLeft());
        cardPanel.getActionMap().put("right", new GoRight());
        cardPanel.getActionMap().put("repeat", new Repeat());
        cardPanel.getActionMap().put("back", new Back());


        setBounds(200, 200, 800, 800);
        setTitle("Breakout");
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Main();
        new Thread(new SoundController("assets/mainSong.wav")).start();
    }

    public void playEasy() {
        map.initEasy();
        paddle.initEasy();
        ball.initDefault();
        controller.init();

        div = 5;
        addCardPanel(view, "GameView");
        showPanel("GameView");
    }

    public void playMedium() {
        map.initMedium();
        paddle.initMedium();
        ball.initDefault();
        controller.init();
        div = 4;
        addCardPanel(view, "GameView");
        showPanel("GameView");
    }

    public void playHard() {
        map.initHard();
        paddle.initHard();
        ball.initDefault();
        controller.init();
        div = 3;
        addCardPanel(view, "GameView");
        showPanel("GameView");
    }

    public void addCardPanel(JPanel cardPanel, String name) {
        Main.cardPanel.add(cardPanel, name);
    }

    public void showPanel(String panel) {
        layout.show(cardPanel, panel);
    }

    public static class GoLeft extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.moveLeft();
        }
    }

    public static class GoRight extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.moveRight();
        }
    }

    public static class Repeat extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (controller.getScore() >= 100) {
                div += 2;
            } else if (controller.getScore() >= 50) {
                div += 1;
            }

            controller.repeatGame();
        }
    }

    public static class Back extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            controller.gameOver();
            layout.show(cardPanel, "Welcome");
        }
    }

}
