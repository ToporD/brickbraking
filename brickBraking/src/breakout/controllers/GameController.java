package breakout.controllers;

import breakout.models.BallModel;
import breakout.models.MapModel;
import breakout.models.PaddleModel;
import breakout.panels.GameView;

import javax.sound.sampled.*;
import java.awt.*;
import java.io.*;
import java.util.Comparator;
import java.util.List;

public class GameController {

    private final MapModel map;
    private final BallModel ball;
    private final PaddleModel paddle;
    private int score;
    private boolean play;
    private int totalBricks;
    private GameView view;
    private List<String> scores;

    public GameController(MapModel map, BallModel ball, PaddleModel paddle) {
        this.map = map;
        this.ball = ball;
        this.paddle = paddle;
    }

    public void init() {
        play = false;

        map.init(map.getRowCount(), map.getColumnCount(), map.getHp());
        totalBricks = map.getTotalBricks();
        score = 0;

        paddle.defaultPosition();
        ball.initDefault();
    }

    public void moveLeft() {
        if (!play) {
            ball.goLeft();
        }
        play = true;
        paddle.moveLeft();
    }

    public void moveRight() {
        if (!play) {
            ball.goRight();
        }
        play = true;
        paddle.moveRight();
    }

    public void repeatGame() {
        if (!play) {
            play = true;
            init();
            view.repaint();
        }
    }

    public void paintElements(Graphics g) {
        map.draw((Graphics2D) g);
        paddle.draw((Graphics2D) g);
        ball.draw((Graphics2D) g);
    }

    public void gameOver() {
        play = false;
        ball.stop();

        try {
            File file = new File("assets/scores.txt");
            FileWriter fr = new FileWriter(file, true);
            BufferedWriter br = new BufferedWriter(fr);
            br.write(getScore() + "\n");
            br.close();
            fr.close();
        } catch (IOException e) {
            System.out.println("Nem kaptam az eredmenyeket tartalmazo fajlt!");
            e.printStackTrace();
        }

        try {
            BufferedReader buf = new BufferedReader(new FileReader("assets/scores.txt"));
            List<String> scores = buf.lines().map(line -> {
                String[] elements = line.split(" ");
                return elements[0];
            }).sorted(Comparator.reverseOrder()).toList();

            setScores(scores);
        } catch (FileNotFoundException e) {
            System.out.println("Nem kaptam az eredmenyeket tartalmazo fajlt!");
            e.printStackTrace();
        }
    }

    public boolean gameFinished() {
        return totalBricks <= 0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    private void ballAndPaddleIntersection() {
        /*
        Paddle physics
                          |          |
       playerX, PADDLE_Y  |          |               playerX + PADDLE_WIDTH, PADDLE_Y
           x______________|__________|______________
            |_____________|__________|_____________|
            left          |  center  |          right
                          |          |

       playerX, PADDLE_Y + PADDLE_HEIGHT            playerX + PADDLE_WIDTH, PADDLE_Y + PADDLE_HEIGHT

        ONE_THIRD_SECTION = PADDLE_WIDTH * 0.33
        */
        Rectangle ballRect = ball.rectangle();
        if (ballRect.intersects(paddle.CenterRectangle())) {          // Center
            ball.flipDirectionY();
        } else if (ballRect.intersects(paddle.LeftRectangle())) {     // Left
            ball.flipDirectionY();
            ball.goLeft();
        } else if (ballRect.intersects(paddle.RightRectangle())) {    // Right
            ball.flipDirectionY();
            ball.goRight();
        }
    }

    public void ballAndBrickCollision(Rectangle brickRect) {
        if (ball.getPozX() + ball.size() - 1 <= brickRect.x || ball.getPozX() + 1 >= brickRect.x + brickRect.width) {
            ball.flipDirectionX();
        } else {
            ball.flipDirectionY();
        }
    }

    public void run() {

        if (play) {

            ballAndPaddleIntersection();

            Rectangle ballRect = ball.rectangle();
            A:
            for (int i = 0; i < map.getRowCount(); ++i) {
                for (int j = 0; j < map.getColumnCount(); ++j) {
                    if (map.getBrickHp(i, j) > 0) {
                        Rectangle brickRect = map.getRectangle(i, j);

                        if (ballRect.intersects(brickRect)) {
                            map.setBrickHp(map.getBrickHp(i, j) - 1, i, j);
                            if (map.getBrickHp(i, j) == 0) {
                                --totalBricks;
                                score += 5;

                                try {
                                    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("assets/breakSound.wav"));
                                    Clip breakSound = AudioSystem.getClip();
                                    breakSound.open(audioInputStream);
                                    breakSound.start();
                                } catch (LineUnavailableException | IOException |
                                         UnsupportedAudioFileException ignore) {
                                }
                            }

                            ballAndBrickCollision(brickRect);
                            ball.nextPosition();

                            break A;
                        }
                    }
                }
            }

            ball.nextPosition();
            ball.collisionWithBorders();
            view.repaint();
        }
    }

    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public void setView(GameView view) {
        this.view = view;
    }
}
