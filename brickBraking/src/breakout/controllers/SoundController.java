package breakout.controllers;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class SoundController implements Runnable {
    private final String fileName;

    public SoundController(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run() {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(fileName));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (UnsupportedAudioFileException | LineUnavailableException | IOException e) {
            System.out.println("IO error");
        }
    }
}
