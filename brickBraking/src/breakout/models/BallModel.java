package breakout.models;

import java.awt.*;

import static java.lang.Math.abs;

public class BallModel {
    private static final int SIZE = 20;
    private Double pozX;
    private Double pozY;
    private Double dirX;
    private Double dirY;

    public BallModel() {

    }

    public void initDefault() {
        pozX = 390.0;
        pozY = 680.0;
        dirX = -2.0;
        dirY = -1.0;
    }

    public void stop() {
        dirX = 0.0;
        dirY = 0.0;
    }

    public boolean outOfBound() {
        return pozY > 770;
    }

    public void collisionWithBorders() {
        // Ball hitting the left side
        if (pozX < 0) {
            dirX = -dirX;
        }

        // Ball hitting the ceiling
        if (pozY < 0) {
            dirY = -dirY;
        }

        // Ball hitting the right side
        if (pozX > 770) {
            dirX = -dirX;
        }
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.gray);
        g.fillOval(pozX.intValue(), pozY.intValue(), SIZE, SIZE);
    }

    public void nextPosition() {
        // iterate to the next position
        pozX += dirX;
        pozY += dirY;
    }

    public Rectangle rectangle() {
        return new Rectangle(pozX.intValue(), pozY.intValue(), SIZE, SIZE);
    }

    public Double getPozX() {
        return pozX;
    }

    public int size() {
        return SIZE;
    }

    public void flipDirectionX() {
        dirX = -dirX;
    }

    public void flipDirectionY() {
        dirY = -dirY;
    }

    public void goLeft() {
        dirX = -abs(dirX);
    }

    public void goRight() {
        dirX = abs(dirX);
    }
}
