package breakout.models;

import java.awt.*;
import java.util.ArrayList;

public class MapModel {

    private static final int WIDTH_OFFSET = 80;
    private static final int HEIGHT_OFFSET = 20;
    private final ArrayList<Color> colors;
    private int row;
    private int col;
    private int brickHp;
    private int totalBricks;
    private int[][] map;
    private int brickWidth;
    private int brickHeight;
    private Rectangle[][] brickRectangles;

    public MapModel() {
        colors = new ArrayList<>();
        colors.add(Color.RED);
        colors.add(Color.ORANGE);
        colors.add(Color.YELLOW);
        colors.add(Color.GREEN);
        colors.add(Color.BLUE);
        colors.add(Color.CYAN);
    }

    public void init(int row, int col, int brickHp) {
        this.row = row;
        this.col = col;
        this.brickHp = brickHp;
        totalBricks = row * col;
        map = new int[row][col];
        brickRectangles = new Rectangle[row][col];

        for (int i = 0; i < map.length; ++i) {
            for (int j = 0; j < map[0].length; ++j) {
                map[i][j] = brickHp;
                brickRectangles[i][j] = new Rectangle(j * brickWidth + WIDTH_OFFSET, i * brickHeight + HEIGHT_OFFSET, brickWidth, brickHeight);
            }
        }

        brickWidth = 600 / col;
        brickHeight = 200 / row;
    }

    public void initEasy() {
        init(5, 7, 1);
    }

    public void initMedium() {
        init(5, 7, 2);
    }

    public void initHard() {
        init(5, 7, 3);
    }

    public void draw(Graphics2D g) {
        for (int i = 0; i < map.length; ++i) {
            for (int j = 0; j < map[0].length; ++j) {
                if (map[i][j] > 0) {
                    // kockak
                    g.setColor(colors.get(i));
                    g.fillRect(brickRectangles[i][j].x, brickRectangles[i][j].y, brickRectangles[i][j].width, brickRectangles[i][j].height);

                    // vonalak
                    g.setStroke(new BasicStroke(3));
                    g.setColor(Color.black);
                    g.drawRect(brickRectangles[i][j].x, brickRectangles[i][j].y, brickRectangles[i][j].width, brickRectangles[i][j].height);
                }
            }
        }
    }

    public int getRowCount() {
        return row;
    }

    public int getColumnCount() {
        return col;
    }

    public Rectangle getRectangle(int i, int j) {
        return brickRectangles[i][j];
    }

    public int getHp() {
        return brickHp;
    }

    public int getBrickHp(int row, int col) {
        return map[row][col];
    }

    public void setBrickHp(int value, int row, int col) {
        map[row][col] = value;
    }

    public int getTotalBricks() {
        return totalBricks;
    }
}
