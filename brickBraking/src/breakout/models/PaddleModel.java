package breakout.models;

import java.awt.*;

public class PaddleModel {

    private static final int MOST_RIGHT = 680;
    private static final int MOST_LEFT = 10;
    private static final int PADDLE_HEIGHT = 8;
    private static final Double PADDLE_Y = 700.0;
    private int PADDLE_WIDTH;
    private Double ONE_THIRD_SECTION = PADDLE_WIDTH * 0.33;
    private Double playerX;

    public PaddleModel() {
    }

    public void defaultPosition() {
        playerX = 350d;
    }

    public void init(double playerX, int paddle_width) {
        this.playerX = playerX;
        PADDLE_WIDTH = paddle_width;
        ONE_THIRD_SECTION = PADDLE_WIDTH * 0.33;
    }

    public void initEasy() {
        init(350d, 100);
    }

    public void initMedium() {
        init(350d, 70);
    }

    public void initHard() {
        init(350d, 50);
    }

      /*
       Paddle physics
                          |          |
       playerX, PADDLE_Y  |          |               playerX + PADDLE_WIDTH, PADDLE_Y
           x______________|__________|______________
            |_____________|__________|_____________|
            left          |  center  |          right
                          |          |

       playerX, PADDLE_Y + PADDLE_HEIGHT            playerX + PADDLE_WIDTH, PADDLE_Y + PADDLE_HEIGHT

        ONE_THIRD_SECTION = PADDLE_WIDTH * 0.33
      */

    public Rectangle CenterRectangle() {
        double centerXStart = playerX + ONE_THIRD_SECTION;
        return new Rectangle((int) centerXStart, PADDLE_Y.intValue(), ONE_THIRD_SECTION.intValue(), PADDLE_HEIGHT);
    }

    public Rectangle LeftRectangle() {
        return new Rectangle(playerX.intValue(), PADDLE_Y.intValue(), ONE_THIRD_SECTION.intValue(), PADDLE_HEIGHT);
    }

    public Rectangle RightRectangle() {
        double rightXStart = playerX + 2 * ONE_THIRD_SECTION;
        return new Rectangle((int) rightXStart, PADDLE_Y.intValue(), PADDLE_WIDTH, PADDLE_HEIGHT);
    }

    public void moveRight() {
        if (playerX >= MOST_RIGHT) {
            playerX = (double) MOST_RIGHT;
        } else {
            playerX += 20;
            if (playerX > MOST_RIGHT)
                playerX = (double) MOST_RIGHT;
        }
    }

    public void moveLeft() {
        if (playerX <= MOST_LEFT) {
            playerX = (double) MOST_LEFT;
        } else {
            playerX -= 20;
        }
    }

    public void draw(Graphics2D g) {
        g.setColor(Color.green);
        g.fillRect(playerX.intValue(), PADDLE_Y.intValue(), PADDLE_WIDTH, PADDLE_HEIGHT);
    }
}
