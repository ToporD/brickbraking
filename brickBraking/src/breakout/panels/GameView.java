package breakout.panels;

import breakout.controllers.GameController;
import breakout.models.BallModel;
import breakout.models.MapModel;
import breakout.models.PaddleModel;

import javax.swing.*;
import java.awt.*;
import java.util.List;
/*
    A kepernyo beallitotsaga
    0, 0                 X  0, 800
    x-------------------------->
    | \
    |    \
    |       \
    |          \
    |             \
    |                \
    |                   \
    v Y                     \
    800, 0                 800, 800

*/

public class GameView extends JPanel {
    private final MapModel map;
    private final BallModel ball;
    private final PaddleModel paddle;
    private final GameController controller;

    public GameView(MapModel map, BallModel ball,
                    PaddleModel paddle, GameController controller) {
        this.map = map;
        this.ball = ball;
        this.paddle = paddle;
        this.controller = controller;

        setFocusable(true);
        requestFocus();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // background
        g.setColor(Color.black);
        g.fillRect(1, 1, 792, 792);

        // scores
        g.setColor(Color.white);
        g.setFont(new Font("Arial", Font.BOLD, 24));
        g.drawString(Integer.toString(controller.getScore()), 700, 30);

        // border
        g.setColor(Color.red);
        g.fillRect(0, 0, 3, 792);
        g.fillRect(0, 0, 792, 3);
        g.fillRect(787, 0, 3, 792);

        controller.paintElements(g);

        if (ball.outOfBound()) {
            controller.gameOver();
            List<String> scores = controller.getScores();

            g.drawString("High Scores:", 100, 80);
            g.drawString(scores.get(0), 100, 100);
            g.drawString(scores.get(1), 100, 150);
            g.drawString(scores.get(2), 100, 200);

            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 34));
            g.drawString("Game Over, Scores: " + controller.getScore(), 300, 300);
            g.drawString("Press Enter to Restart", 350, 350);
        }

        if (controller.gameFinished()) {
            controller.gameOver();

            g.setColor(Color.red);
            g.setFont(new Font("Arial", Font.BOLD, 34));
            g.drawString("You won! Scores: " + controller.getScore(), 300, 300);
            g.drawString("Press Enter to Restart", 350, 350);
        }
    }
}
