package breakout.panels;

import javax.swing.*;
import java.awt.*;


public class WelcomePanel extends JPanel {

    private final JButton easy;
    private final JButton medium;
    private final JButton hard;

    public WelcomePanel() {
        setLayout(new GridLayout(5, 1));
        JLabel title = new JLabel("Welcome to Breakout!");
        easy = new JButton("Easy Mode");
        medium = new JButton("Medium Mode");
        hard = new JButton("Hard Mode");

        add(title);
        add(easy);
        add(medium);
        add(hard);
    }

    public JButton getEasyButton() {
        return easy;
    }

    public JButton getMediumButton() {
        return medium;
    }

    public JButton getHardButton() {
        return hard;
    }
}
